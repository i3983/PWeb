package ro.pweb.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.Donation;
import ro.pweb.backend.model.Recipient;
import ro.pweb.backend.model.Requirement;
import ro.pweb.backend.repository.RecipientRepository;
import ro.pweb.backend.response.RecipientResponse;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RecipientServiceTest {

    @Mock
    private RecipientRepository recipientRepository;

    @InjectMocks
    private RecipientService recipientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetRecipientById() {
        // Arrange
        int recipientId = 1;
        Recipient mockRecipient = createMockRecipient(recipientId);
        when(recipientRepository.findById(recipientId)).thenReturn(java.util.Optional.of(mockRecipient));

        // Act
        RecipientResponse response = recipientService.getRecipientById(recipientId);

        // Assert
        assertEquals(mockRecipient.getId(), response.getId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetRecipientById_RecipientNotFound() {
        // Arrange
        int recipientId = 1;
        when(recipientRepository.findById(recipientId)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(NoSuchElementFoundException.class,
                () -> recipientService.getRecipientById(recipientId));
    }

    @Test
    void testGetRecipientByEmail() {
        // Arrange
        String email = "test@example.com";
        Recipient mockRecipient = createMockRecipient(1);
        when(recipientRepository.findRecipientByEmail(email)).thenReturn(mockRecipient);

        // Act
        RecipientResponse response = recipientService.getRecipientByEmail(email);

        // Assert
        assertEquals(mockRecipient.getId(), response.getId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetRecipientByEmail_RecipientNotFound() {
        // Arrange
        String email = "nonexistent@example.com";
        when(recipientRepository.findRecipientByEmail(email)).thenReturn(null);

        // Act and Assert
        assertThrows(NoSuchElementFoundException.class,
                () -> recipientService.getRecipientByEmail(email));
    }

    @Test
    void testSubscribeNewsletter() {
        // Arrange
        int recipientId = 1;
        Recipient mockRecipient = createMockRecipient(recipientId);
        when(recipientRepository.subscribeNewsletter(recipientId)).thenReturn(1);
        when(recipientRepository.findById(recipientId)).thenReturn(java.util.Optional.of(mockRecipient));

        // Act
        RecipientResponse response = recipientService.subscribeNewsletter(recipientId);

        // Assert
        assertEquals(mockRecipient.getId(), response.getId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testSubscribeNewsletter_RecipientNotFound() {
        // Arrange
        int recipientId = 1;
        when(recipientRepository.subscribeNewsletter(recipientId)).thenReturn(0);

        // Act and Assert
        assertThrows(NoSuchElementFoundException.class,
                () -> recipientService.subscribeNewsletter(recipientId));
    }

    // Similar tests for unsubscribeNewsletter and saveRecipient can be added

    private Recipient createMockRecipient(int id) {
        Recipient recipient = new Recipient();
        recipient.setId(id);
        recipient.setIdentityGuid("some-guid");
        recipient.setRecipientEmail("test@example.com");
        recipient.setPhoneNumber("123456789");
        recipient.setAddress("Test Address");
        recipient.setName("Test Recipient");
        recipient.setSubscribedDonations(true);

        // Create and set requirements
        List<Requirement> requirements = new ArrayList<>();
        Requirement requirement1 = new Requirement();
        // Set properties of requirement1
        requirements.add(requirement1);

        Requirement requirement2 = new Requirement();
        // Set properties of requirement2
        requirements.add(requirement2);

        recipient.setRequirements(requirements);

        // Create and set donations
        List<Donation> donations = new ArrayList<>();
        Donation donation1 = new Donation();
        // Set properties of donation1
        donations.add(donation1);

        Donation donation2 = new Donation();
        // Set properties of donation2
        donations.add(donation2);

        recipient.setDonations(donations);

        return recipient;
    }
}

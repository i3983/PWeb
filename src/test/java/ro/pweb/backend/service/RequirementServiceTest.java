package ro.pweb.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.*;
import ro.pweb.backend.repository.DonorRepository;
import ro.pweb.backend.repository.RecipientRepository;
import ro.pweb.backend.repository.RequirementRepository;
import ro.pweb.backend.response.RequirementResponse;
import ro.pweb.backend.response.RequirementShortResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class RequirementServiceTest {

    @Mock
    private RequirementRepository requirementRepository;

    @Mock
    private RecipientRepository recipientRepository;

    @Mock
    private DonorRepository donorRepository;

    @Mock
    private RabbitTemplate rabbitTemplate;

    @InjectMocks
    private RequirementService requirementService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSaveRequirement() {
        // Arrange
        Requirement mockRequirement = createMockRequirement(1);
        when(requirementRepository.save(any(Requirement.class))).thenReturn(mockRequirement);

        // Act
        RequirementResponse response = requirementService.saveRequirement(mockRequirement);

        // Assert
        assertEquals(mockRequirement.getRequirementId(), response.getRequirementId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetRequirementById() {
        // Arrange
        int requirementId = 1;
        Requirement mockRequirement = createMockRequirement(requirementId);
        when(requirementRepository.findById(requirementId)).thenReturn(java.util.Optional.of(mockRequirement));

        // Act
        RequirementResponse response = requirementService.getRequirementById(requirementId);

        // Assert
        assertEquals(mockRequirement.getRequirementId(), response.getRequirementId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetRequirementById_RequirementNotFound() {
        // Arrange
        int requirementId = 1;
        when(requirementRepository.findById(requirementId)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(NoSuchElementFoundException.class,
                () -> requirementService.getRequirementById(requirementId));
    }

    @Test
    void testGetRequirementByRecipientId() {
        // Arrange
        int recipientId = 1;
        Recipient mockRecipient = createMockRecipient(recipientId);
        when(recipientRepository.findById(recipientId)).thenReturn(java.util.Optional.of(mockRecipient));
        when(requirementRepository.findRequirementByRecipientId(recipientId)).thenReturn(new ArrayList<>());

        // Act
        List<RequirementShortResponse> response = requirementService.getRequirementByRecipientId(recipientId);

        // Assert
        assertNotNull(response);
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetActiveRequirements() {
        // Arrange
        when(requirementRepository.findActiveRequirements()).thenReturn(new ArrayList<>());

        // Act
        List<RequirementShortResponse> response = requirementService.getActiveRequirements();

        // Assert
        assertNotNull(response);
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetDonorClaimedRequirements() {
        // Arrange
        int donorId = 1;
        Donor mockDonor = createMockDonor(donorId);
        when(donorRepository.findById(donorId)).thenReturn(java.util.Optional.of(mockDonor));
        when(requirementRepository.findDonorClaimedRequirements(donorId)).thenReturn(new ArrayList<>());

        // Act
        List<RequirementShortResponse> response = requirementService.getDonorClaimedRequirements(donorId);

        // Assert
        assertNotNull(response);
        // You may add more assertions based on your specific requirements
    }

    // Add more tests for claimRequirement, recipientCloseRequirement, donorCloseRequirement methods

    private Requirement createMockRequirement(int requirementId) {
        Requirement requirement = new Requirement();
        requirement.setRequirementId(requirementId);
        requirement.setPostTitle("Test Requirement");
        requirement.setPublishDate(new Date());
        requirement.setStatus(Status.ACTIVE);
        requirement.setItems(createTestItems());
        requirement.setDonor(createMockDonor(1));
        requirement.setRecipient(createMockRecipient(1));

        return requirement;
    }
    private static List<ItemRequirement> createTestItems() {
        List<ItemRequirement> items = new ArrayList<>();
        // Add test items as needed
        items.add(createTestItem("Item1", 10));
        items.add(createTestItem("Item2", 5));
        return items;
    }

    private static ItemRequirement createTestItem(String itemName, double quantity) {
        ItemRequirement item = new ItemRequirement();
        item.setItemName(itemName);
        item.setQuantity(quantity);
        // Set other item properties as needed
        return item;
    }

    private Recipient createMockRecipient(int recipientId) {
        // Create and return a mock Recipient object for testing
        Recipient recipient = new Recipient();
        recipient.setId(recipientId);
        // Set other properties as needed
        return recipient;
    }

    private Donor createMockDonor(int donorId) {
        // Create and return a mock Donor object for testing
        Donor donor = new Donor();
        donor.setId(donorId);
        // Set other properties as needed
        return donor;
    }
}

package ro.pweb.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.Donor;
import ro.pweb.backend.repository.DonorRepository;
import ro.pweb.backend.response.DonorResponse;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class DonorServiceTest {

    @Mock
    private DonorRepository donorRepository;

    @InjectMocks
    private DonorService donorService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetDonorById() {
        // Arrange
        int donorId = 1;
        Donor mockDonor = createMockDonor(donorId);
        when(donorRepository.findById(donorId)).thenReturn(java.util.Optional.of(mockDonor));

        // Act
        DonorResponse response = donorService.getDonorById(donorId);

        // Assert
        assertEquals(mockDonor.getId(), response.getId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetDonorById_DonorNotFound() {
        // Arrange
        int donorId = 1;
        when(donorRepository.findById(donorId)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(NoSuchElementFoundException.class,
                () -> donorService.getDonorById(donorId));
    }

    @Test
    void testSaveDonor_NewDonor() {
        // Arrange
        Donor newDonor = createMockDonor(1);
        when(donorRepository.findDonorByEmail(newDonor.getDonorEmail())).thenReturn(null);
        when(donorRepository.save(newDonor)).thenReturn(newDonor);

        // Act
        DonorResponse response = donorService.saveDonor(newDonor);

        // Assert
        assertEquals(newDonor.getId(), response.getId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testSaveDonor_ExistingDonor() {
        // Arrange
        Donor existingDonor = createMockDonor(1);
        when(donorRepository.findDonorByEmail(existingDonor.getDonorEmail())).thenReturn(existingDonor);

        // Act
        DonorResponse response = donorService.saveDonor(existingDonor);

        // Assert
        assertEquals(existingDonor.getId(), response.getId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetDonorByEmail() {
        // Arrange
        String email = "test@example.com";
        Donor mockDonor = createMockDonor(1);
        when(donorRepository.findDonorByEmail(email)).thenReturn(mockDonor);

        // Act
        DonorResponse response = donorService.getDonorByEmail(email);

        // Assert
        assertEquals(mockDonor.getId(), response.getId());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetDonorByEmail_DonorNotFound() {
        // Arrange
        String email = "nonexistent@example.com";
        when(donorRepository.findDonorByEmail(email)).thenReturn(null);

        // Act and Assert
        assertThrows(NoSuchElementFoundException.class,
                () -> donorService.getDonorByEmail(email));
    }

    private Donor createMockDonor(int donorId) {
        // Create and return a mock Donor object for testing
        Donor donor = new Donor();
        donor.setId(donorId);
        // Set other properties as needed
        return donor;
    }
}

package ro.pweb.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.*;
import ro.pweb.backend.repository.DonationRepository;
import ro.pweb.backend.repository.DonorRepository;
import ro.pweb.backend.repository.RecipientRepository;
import ro.pweb.backend.response.DonationResponse;
import ro.pweb.backend.response.DonationShortResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DonationServiceTest {

    @Mock
    private DonationRepository donationRepository;

    @Mock
    private DonorRepository donorRepository;

    @Mock
    private RecipientRepository recipientRepository;

    @Mock
    private RabbitTemplate template;

    @InjectMocks
    private DonationService donationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSaveDonation() {
        // Arrange
        Donation donation = createTestDonation();
        when(donationRepository.save(any(Donation.class))).thenReturn(donation);

        // Act
        DonationResponse response = donationService.saveDonation(donation);

        // Assert
        verify(donationRepository).save(any(Donation.class));
        assertEquals(Status.ACTIVE, donation.getStatus()); // Make additional assertions as needed
    }

    @Test
    void testGetActiveDonations() {
        // Arrange
        List<Donation> mockDonations = createMockDonations();
        when(donationRepository.findActiveDonations()).thenReturn(mockDonations);

        // Act
        List<DonationShortResponse> response = donationService.getActiveDonations();

        // Assert
        assertEquals(mockDonations.size(), response.size());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetDonationsByDonorId() {
        // Arrange
        int donorId = 1;
        Donor mockDonor = createMockDonor(donorId);
        when(donorRepository.findById(donorId)).thenReturn(Optional.of(mockDonor));

        List<Donation> mockDonations = createMockDonations();
        when(donationRepository.findDonationByDonorId(donorId)).thenReturn(mockDonations);

        // Act
        List<DonationShortResponse> response = donationService.getDonationsByDonorId(donorId);

        // Assert
        assertEquals(mockDonations.size(), response.size());
        // You may add more assertions based on your specific requirements
    }

    @Test
    void testGetDonationsByDonorId_DonorNotFound() {
        // Arrange
        int donorId = 1;
        when(donorRepository.findById(donorId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(NoSuchElementFoundException.class,
                () -> donationService.getDonationsByDonorId(donorId));
    }

    private Donor createMockDonor(int donorId) {
        // Create and return a mock Donor object for testing
        Donor donor = new Donor();
        donor.setId(donorId);
        // Set other properties as needed
        return donor;
    }

    private List<Donation> createMockDonations() {
        // Create and return a list of mock Donation objects for testing
        // Set their status to ACTIVE to simulate active donations
        List<Donation> donations = new ArrayList<>();
        Donation donation1 = createTestDonation();
        // Set other properties as needed
        donations.add(donation1);

        Donation donation2 = createTestDonation();
        // Set other properties as needed
        donations.add(donation2);

        return donations;
    }

    // You can write similar tests for other methods in DonationService

    private Donation createTestDonation() {
        Donation donation = new Donation();
        donation.setDonationId(1);
        donation.setPostTitle("Test Donation");
        donation.setPublishDate(new Date());
        donation.setStatus(Status.ACTIVE); // Assuming you have an enum named Status

        // Create and set items
        List<ItemDonation> items = new ArrayList<>();
        ItemDonation item1 = new ItemDonation();
        // Set properties of item1
        items.add(item1);

        ItemDonation item2 = new ItemDonation();
        // Set properties of item2
        items.add(item2);

        donation.setItems(items);

        // Create and set donor
        Donor donor = new Donor();
        // Set properties of donor
        donation.setDonor(donor);

        // Create and set recipient
        Recipient recipient = new Recipient();
        // Set properties of recipient
        donation.setRecipient(recipient);

        return donation;
    }
}
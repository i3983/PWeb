package ro.pweb.backend.expections;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class OperationNotPermitedException extends ResponseStatusException {

    public OperationNotPermitedException(String message) {
        super(HttpStatus.METHOD_NOT_ALLOWED, message);
    }
}

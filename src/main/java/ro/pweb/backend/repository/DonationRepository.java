package ro.pweb.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ro.pweb.backend.model.Donation;
import ro.pweb.backend.model.Requirement;
import ro.pweb.backend.model.Status;

import java.util.List;

public interface DonationRepository extends JpaRepository<Donation, Integer> {
    @Query("SELECT u FROM Donation u WHERE u.donor.id = ?1")
    public List<Donation> findDonationByDonorId(int id);

    @Query("SELECT u FROM Donation u WHERE u.status = 'ACTIVE'")
    public List<Donation> findActiveDonations();

    @Query("SELECT u FROM Donation u WHERE u.recipient.id = ?1")
    public List<Donation> findRecipientClaimedDonations(int id);

    @Query("SELECT u.status FROM Donation u WHERE u.donationId = ?1")
    public Status getStatus(int id);

    @Modifying
    @Query("update Donation u set u.status = 'PENDING' where u.donationId = ?1")
    public int claimDonation(int id);

    @Modifying
    @Query("update Donation u set u.recipient.id = ?2 where u.donationId = ?1")
    public int addRecipient(int id, int recipientId);

    @Modifying
    @Query("update Donation u set u.status = 'CLOSED_BY_RECIPIENT' where u.donationId = ?1")
    public int recipientCloseDonation(int id);

    @Modifying
    @Query("update Donation u set u.status = 'CLOSED' where u.donationId = ?1")
    public int donorCloseDonation(int id);
}

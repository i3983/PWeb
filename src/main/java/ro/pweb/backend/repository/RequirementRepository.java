package ro.pweb.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ro.pweb.backend.model.Recipient;
import ro.pweb.backend.model.Requirement;
import ro.pweb.backend.model.Status;

import java.util.List;

public interface RequirementRepository extends JpaRepository<Requirement, Integer> {

    @Query("SELECT u FROM Requirement u WHERE u.recipient.id = ?1")
    public List<Requirement> findRequirementByRecipientId(int id);

    @Query("SELECT u FROM Requirement u WHERE u.status = 'ACTIVE'")
    public List<Requirement> findActiveRequirements();

    @Query("SELECT u FROM Requirement u WHERE u.donor.id = ?1")
    public List<Requirement> findDonorClaimedRequirements(int id);

    @Query("SELECT u.status FROM Requirement u WHERE u.requirementId = ?1")
    public Status getStatus(int id);

    @Modifying
    @Query("update Requirement u set u.status = 'PENDING' where u.requirementId = ?1")
    public int claimRequirement(int id);

    @Modifying
    @Query("update Requirement u set u.donor.id = ?2 where u.requirementId = ?1")
    public int addDonor(int id, int donorId);

    @Modifying
    @Query("update Requirement u set u.status = 'CLOSED_BY_RECIPIENT' where u.requirementId = ?1")
    public int recipientCloseRequirement(int id);

    @Modifying
    @Query("update Requirement u set u.status = 'CLOSED' where u.requirementId = ?1")
    public int donorCloseRequirement(int id);
}

package ro.pweb.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.pweb.backend.model.ItemRequirement;
import ro.pweb.backend.model.Requirement;

public interface ItemRequirementRepository extends JpaRepository<ItemRequirement, Integer> {
}

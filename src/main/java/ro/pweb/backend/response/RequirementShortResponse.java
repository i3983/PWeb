package ro.pweb.backend.response;

import ro.pweb.backend.model.Status;

import java.util.Date;
import java.util.List;

public class RequirementShortResponse {
    private int requirementId;

    private String postTitle;

    private String publishDate;

    private Status status;

    private String recipientName;

    private int numberItemsRequested;

    public int getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(int requirementId) {
        this.requirementId = requirementId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public int getNumberItemsRequested() {
        return numberItemsRequested;
    }

    public void setNumberItemsRequested(int numberItemsRequested) {
        this.numberItemsRequested = numberItemsRequested;
    }
}

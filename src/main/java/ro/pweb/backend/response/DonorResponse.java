package ro.pweb.backend.response;

public class DonorResponse {
    private int id;

    private String identityGuid;

    private String donorEmail;

    private String phoneNumber;

    private String address;

    private String name;

    public boolean subscribedRequirements;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentityGuid() {
        return identityGuid;
    }

    public void setIdentityGuid(String identityGuid) {
        this.identityGuid = identityGuid;
    }

    public String getDonorEmail() {
        return donorEmail;
    }

    public void setDonorEmail(String donorEmail) {
        this.donorEmail = donorEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSubscribedRequirements() {
        return subscribedRequirements;
    }

    public void setSubscribedRequirements(boolean subscribedRequirements) {
        this.subscribedRequirements = subscribedRequirements;
    }
}

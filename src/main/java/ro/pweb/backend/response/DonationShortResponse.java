package ro.pweb.backend.response;

import ro.pweb.backend.model.Status;

import java.util.Date;

public class DonationShortResponse {
    private int donationId;

    private String postTitle;

    private String publishDate;

    private Status status;

    private String donorName;

    private int numberItemsRequested;

    public int getDonationId() {
        return donationId;
    }

    public void setDonationId(int donationId) {
        this.donationId = donationId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDonorName() {
        return donorName;
    }

    public void setDonorName(String donorName) {
        this.donorName = donorName;
    }

    public int getNumberItemsRequested() {
        return numberItemsRequested;
    }

    public void setNumberItemsRequested(int numberItemsRequested) {
        this.numberItemsRequested = numberItemsRequested;
    }
}

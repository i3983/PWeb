package ro.pweb.backend.response;

public class RecipientResponse {
    private int id;

    private String identityGuid;

    private String recipientEmail;

    private String phoneNumber;

    private String address;

    private String name;

    public boolean subscribedDonations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentityGuid() {
        return identityGuid;
    }

    public void setIdentityGuid(String identityGuid) {
        this.identityGuid = identityGuid;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSubscribedDonations() {
        return subscribedDonations;
    }

    public void setSubscribedDonations(boolean subscribedDonations) {
        this.subscribedDonations = subscribedDonations;
    }
}

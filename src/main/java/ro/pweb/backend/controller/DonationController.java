package ro.pweb.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.expections.OperationNotPermitedException;
import ro.pweb.backend.model.Donation;
import ro.pweb.backend.model.Requirement;
import ro.pweb.backend.repository.DonationRepository;
import ro.pweb.backend.response.*;
import ro.pweb.backend.service.DonationService;
import ro.pweb.backend.service.RequirementService;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/donation")
public class DonationController {
    Logger logger = LoggerFactory.getLogger(DonationController.class);

    @Resource(name = "donationService")
    private DonationService donationService;

    @PostMapping
    public DonationResponse saveDonation(final @RequestBody Donation donation){
        logger.info("Donation " + donation.toString());
        return donationService.saveDonation(donation);
    }

    @GetMapping("/{id}")
    public DonationResponse getDonation(@PathVariable int id){
        logger.info("Get donation by id");
        return donationService.getDonationById(id);
    }

    @GetMapping("/donor/{id}")
    public List<DonationShortResponse> getDonationsByDonorId(@PathVariable int id){
        logger.info("Get donations by donor id");
        return donationService.getDonationsByDonorId(id);
    }

    @GetMapping("/active")
    public List<DonationShortResponse> getActiveDonations(){
        logger.info("Get active donations");
        return donationService.getActiveDonations();
    }

    @GetMapping("/recipient/{id}")
    public List<DonationShortResponse> getRecipientClaimedDonations(@PathVariable int id){
        logger.info("Get all donations claimed by recipient with id");
        return donationService.getRecipientClaimedDonations(id);
    }

    @PutMapping("/claim")
    public DonationResponse claimDonation(final @RequestBody Donation donation){
        logger.info("claim donation");
        return donationService.claimDonation(donation);
    }

    @PutMapping("/recipient/close")
    public DonationResponse recipientCloseDonation(final @RequestBody Donation donation){
        logger.info("close donation by recipient");
        return donationService.recipientCloseDonation(donation);
    }

    @PutMapping("/donor/close")
    public DonationResponse donorCloseDonation(final @RequestBody Donation donation){
        logger.info("close donation by donor");
        return donationService.donorCloseDonation(donation);
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleItemNotFoundException(
            NoSuchElementFoundException exception,
            WebRequest request
    ){
        logger.error("Failed to find the requested element", exception);
        logger.error("Message : " + exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                exception.getMessage()
        );

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(OperationNotPermitedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<ErrorResponse> handleItemNotFoundException(
            OperationNotPermitedException exception,
            WebRequest request
    ){
        logger.error("Failed to find the requested element", exception);
        logger.error("Message : " + exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                HttpStatus.METHOD_NOT_ALLOWED.value(),
                exception.getMessage()
        );

        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(errorResponse);
    }
}

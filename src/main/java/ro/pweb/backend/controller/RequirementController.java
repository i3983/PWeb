package ro.pweb.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.expections.OperationNotPermitedException;
import ro.pweb.backend.model.Requirement;
import ro.pweb.backend.response.ErrorResponse;
import ro.pweb.backend.response.RequirementResponse;
import ro.pweb.backend.response.RequirementShortResponse;
import ro.pweb.backend.service.RequirementService;;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/requirement")
public class RequirementController {

    Logger logger = LoggerFactory.getLogger(RequirementController.class);

    @Resource(name = "requirementService")
    private RequirementService requirementService;

    @PostMapping
    public RequirementResponse saveRequirement(final @RequestBody Requirement requirement){
        logger.info("Requirement" + requirement.toString());
        return requirementService.saveRequirement(requirement);
    }

    @GetMapping("/{id}")
    public RequirementResponse getRequirement(@PathVariable int id){
        logger.info("Get requirement by id");
        return requirementService.getRequirementById(id);
    }

    @GetMapping("/recipient/{id}")
    public List<RequirementShortResponse> getRequirementByRecipientId(@PathVariable int id){
        logger.info("Get requirement by recipient email");
        return requirementService.getRequirementByRecipientId(id);
    }

    @GetMapping("/active")
    public List<RequirementShortResponse> getActiveRequirements(){
        logger.info("Get active requirements");
        return requirementService.getActiveRequirements();
    }

    @GetMapping("/donor/{id}")
    public List<RequirementShortResponse> getDonorClaimedRequirements(@PathVariable int id){
        logger.info("Get all requirements claimed by donor with id");
        return requirementService.getDonorClaimedRequirements(id);
    }

    @PutMapping("/claim")
    public RequirementResponse claimRequirement(final @RequestBody Requirement requirement){
        logger.info("claim requirement");
        return requirementService.claimRequirement(requirement);
    }

    @PutMapping("/recipient/close")
    public RequirementResponse recipientCloseRequirement(final @RequestBody Requirement requirement){
        logger.info("close requirement by recipient");
        return requirementService.recipientCloseRequirement(requirement);
    }

    @PutMapping("/donor/close")
    public RequirementResponse donorCloseRequirement(final @RequestBody Requirement requirement){
        logger.info("close requirement by donor");
        return requirementService.donorCloseRequirement(requirement);
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleItemNotFoundException(
            NoSuchElementFoundException exception,
            WebRequest request
    ){
        logger.error("Failed to find the requested element", exception);
        logger.error("Message : " + exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                exception.getMessage()
        );

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(OperationNotPermitedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<ErrorResponse> handleItemNotFoundException(
            OperationNotPermitedException exception,
            WebRequest request
    ){
        logger.error("Failed to find the requested element", exception);
        logger.error("Message : " + exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                HttpStatus.METHOD_NOT_ALLOWED.value(),
                exception.getMessage()
        );

        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(errorResponse);
    }
}

package ro.pweb.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.Recipient;
import ro.pweb.backend.response.DonorResponse;
import ro.pweb.backend.response.ErrorResponse;
import ro.pweb.backend.response.RecipientResponse;
import ro.pweb.backend.service.RecipientService;


import javax.annotation.Resource;

@RestController
@RequestMapping("/recipient")
public class RecipientController {
    Logger logger = LoggerFactory.getLogger(RecipientController.class);

    @Resource(name = "recipientService")
    private RecipientService recipientService;

    @GetMapping("/id/{id}")
    public RecipientResponse getRecipient(@PathVariable int id){
        logger.info("Get recipient by id");
        return recipientService.getRecipientById(id);
    }

    @GetMapping("/email/{email}")
    public RecipientResponse getRecipientByEmail(@PathVariable String email){
        logger.info("Get recipient by email");
        return recipientService.getRecipientByEmail(email);
    }

    @PostMapping
    public RecipientResponse saveCustomer(final @RequestBody Recipient recipient){
        logger.info("Recipient" + recipient.toString());
        return recipientService.saveRecipient(recipient);
    }

    @DeleteMapping("/{id}")
    public Boolean deleteDonor(@PathVariable int id){
        return recipientService.deleteCustomer(id);
    }

    @PutMapping("/subscribe/{id}")
    public RecipientResponse subscribeNewsletter(@PathVariable int id){
        logger.info("recipient subscribe to newsletter");
        return recipientService.subscribeNewsletter(id);
    }

    @PutMapping("/unsubscribe/{id}")
    public RecipientResponse unsubscribeNewsletter(@PathVariable int id){
        logger.info("recipient unsubscribe to newsletter");
        return recipientService.unsubscribeNewsletter(id);
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleItemNotFoundException(
            NoSuchElementFoundException exception,
            WebRequest request
    ){
        logger.error("Failed to find the requested element", exception);
        logger.error("Message : " + exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                exception.getMessage()
        );

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }
}

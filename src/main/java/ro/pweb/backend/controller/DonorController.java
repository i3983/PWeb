package ro.pweb.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.Donor;
import ro.pweb.backend.model.Requirement;
import ro.pweb.backend.response.DonorResponse;
import ro.pweb.backend.response.ErrorResponse;
import ro.pweb.backend.response.RequirementResponse;
import ro.pweb.backend.service.DonorService;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/donor")
public class DonorController {

    Logger logger = LoggerFactory.getLogger(DonorController.class);

    @Resource(name = "donorService")
    private DonorService donorService;

    @GetMapping("/id/{id}")
    public DonorResponse getDonorById(@PathVariable int id){
        logger.info("Get donor by id");
        return donorService.getDonorById(id);
    }

    @GetMapping("/email/{email}")
    public DonorResponse getDonorByEmail(@PathVariable String email){
        logger.info("Get donor by email");
        return donorService.getDonorByEmail(email);
    }

    @PostMapping
    public DonorResponse saveCustomer(final @RequestBody Donor donor){
        logger.info("Donorrr" + donor.toString());
        return donorService.saveDonor(donor);
    }

    @PutMapping("/subscribe/{id}")
    public DonorResponse subscribeNewsletter(@PathVariable int id){
        logger.info("donor subscribe to newsletter");
        return donorService.subscribeNewsletter(id);
    }

    @PutMapping("/unsubscribe/{id}")
    public DonorResponse unsubscribeNewsletter(@PathVariable int id){
        logger.info("donor unsubscribe to newsletter");
        return donorService.unsubscribeNewsletter(id);
    }


    @DeleteMapping("/{id}")
    public Boolean deleteDonor(@PathVariable int id){
        return donorService.deleteCustomer(id);
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleItemNotFoundException(
            NoSuchElementFoundException exception,
            WebRequest request
    ){
        logger.error("Failed to find the requested element", exception);
        logger.error("Message : " + exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                exception.getMessage()
        );

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }
}

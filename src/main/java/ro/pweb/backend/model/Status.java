package ro.pweb.backend.model;

public enum Status {
    ACTIVE,
    PENDING,
    CLOSED_BY_RECIPIENT,
    CLOSED
}

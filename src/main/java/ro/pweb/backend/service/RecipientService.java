package ro.pweb.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.Recipient;
import ro.pweb.backend.response.DonorResponse;
import ro.pweb.backend.response.RecipientResponse;
import ro.pweb.backend.repository.RecipientRepository;


import javax.persistence.EntityNotFoundException;

@Service("recipientService")
public class RecipientService {
    @Autowired
    private RecipientRepository recipientRepository;

    Logger logger = LoggerFactory.getLogger(RecipientService.class);

    public RecipientResponse getRecipientById(int recipientId){
        Recipient recipient =  recipientRepository.findById(recipientId).orElseThrow(() -> new NoSuchElementFoundException("Recipient not found"));
        return createRecipientResponse(recipient);
    }

    public RecipientResponse getRecipientByEmail(String email){
        Recipient recipient = recipientRepository.findRecipientByEmail(email);
        if (recipient == null){
            throw new NoSuchElementFoundException("Recipient not found");
        }
        return createRecipientResponse(recipient);

    }

    @Transactional
    public RecipientResponse subscribeNewsletter(int id){
        int succes = recipientRepository.subscribeNewsletter(id);
        if (succes == 1){
            return getRecipientById(id);
        }
        throw new NoSuchElementFoundException("Recipient not found");
    }

    @Transactional
    public RecipientResponse unsubscribeNewsletter(int id){
        int succes = recipientRepository.unsubscribeNewsletter(id);
        if (succes == 1){
            return getRecipientById(id);
        }
        throw new NoSuchElementFoundException("Recipient not found");
    }

    public RecipientResponse saveRecipient(Recipient recipient){
        Recipient existingRecipient = recipientRepository.findRecipientByEmail(recipient.getRecipientEmail());
        if (existingRecipient == null) {
            logger.info("Adding new recipient");
            existingRecipient = recipientRepository.save(recipient);
        }
        logger.info("Recipient already exists");
        return createRecipientResponse(existingRecipient);
    }

    public boolean deleteCustomer(int recipientId){
        recipientRepository.deleteById(recipientId);
        return true;
    }

    public RecipientResponse createRecipientResponse(Recipient recipient){
        RecipientResponse response = new RecipientResponse();
        response.setId(recipient.getId());
        response.setAddress(recipient.getAddress());
        response.setPhoneNumber(recipient.getPhoneNumber());
        response.setRecipientEmail(recipient.getRecipientEmail());
        response.setSubscribedDonations(recipient.isSubscribedDonations());
        response.setName(recipient.getName());
        return response;
    }
}

package ro.pweb.backend.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.pweb.backend.consumer.Notification;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.expections.OperationNotPermitedException;
import ro.pweb.backend.model.*;
import ro.pweb.backend.repository.DonationRepository;
import ro.pweb.backend.repository.DonorRepository;
import ro.pweb.backend.repository.RecipientRepository;
import ro.pweb.backend.response.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@Service("donationService")
public class DonationService {

    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private DonorRepository donorRepository;

    @Autowired
    private RecipientRepository recipientRepository;

    @Autowired
    private RabbitTemplate template;

    public DonationResponse saveDonation(Donation donation){
        donation.setStatus(Status.ACTIVE);
        for (ItemDonation i : donation.getItems()){
            i.setDonation(donation);
        }
        Donation savedDonation = donationRepository.save(donation);
        DonationResponse response = createDonationResponse(savedDonation);
        Notification notification = new Notification("donation", response);
        template.convertAndSend("pweb-exchange", "pweb-key", notification );
        return response;
    }

    public DonationResponse getDonationById(int donationId){
        Donation donation = donationRepository.findById(donationId).orElseThrow(() -> new NoSuchElementFoundException("Donation not found"));
        return createDonationResponse(donation);
    }

    public List<DonationShortResponse> getDonationsByDonorId(int id){
        Donor donor = donorRepository.findById(id).orElseThrow(() -> new NoSuchElementFoundException("Donor not found"));
        List<Donation> donations = donationRepository.findDonationByDonorId(id);
        List<DonationShortResponse> response = new ArrayList<>();
        if (donations != null) {
            for (Donation r : donations) {
                response.add(createDonationShortResponse(r));
            }
        }
        return response;
    }

    public List<DonationShortResponse> getActiveDonations(){
        List<Donation> donations = donationRepository.findActiveDonations();
        List<DonationShortResponse> response = new ArrayList<>();
        for (Donation r : donations){
            response.add(createDonationShortResponse(r));
        }
        return response;
    }

    public List<DonationShortResponse> getRecipientClaimedDonations(int id){
        recipientRepository.findById(id).orElseThrow(() -> new NoSuchElementFoundException("Recipient not found"));
        List<Donation> donations = donationRepository.findRecipientClaimedDonations(id);
        List<DonationShortResponse> response = new ArrayList<>();
        for (Donation r : donations){
            response.add(createDonationShortResponse(r));
        }
        return response;
    }

    @Transactional
    public DonationResponse claimDonation(Donation donation){
        Status donationStatus = donationRepository.getStatus(donation.getDonationId());
        if (donationStatus != Status.ACTIVE){
            throw new OperationNotPermitedException("Donation is not in active status");
        }
        int succes = donationRepository.claimDonation(donation.getDonationId());
        if (succes == 1){
            succes = donationRepository.addRecipient(donation.getDonationId(), donation.getRecipient().getId());
            return getDonationById(donation.getDonationId());
        }
        throw new NoSuchElementFoundException("Donation not found");
    }

    @Transactional
    public DonationResponse recipientCloseDonation(Donation donation){
        Status donationStatus = donationRepository.getStatus(donation.getDonationId());
        if (donationStatus != Status.PENDING){
            throw new OperationNotPermitedException("Donation is not in pending status");
        }
        int succes = donationRepository.recipientCloseDonation(donation.getDonationId());
        if (succes == 1){
            return getDonationById(donation.getDonationId());
        }
        throw new NoSuchElementFoundException("Donation not found");
    }

    @Transactional
    public DonationResponse donorCloseDonation(Donation donation){
        Status donationStatus = donationRepository.getStatus(donation.getDonationId());
        if (donationStatus != Status.CLOSED_BY_RECIPIENT){
            throw new OperationNotPermitedException("Donation is not in closed_by_recipient status");
        }
        int succes = donationRepository.donorCloseDonation(donation.getDonationId());
        if (succes == 1){
            return getDonationById(donation.getDonationId());
        }
        throw new NoSuchElementFoundException("Donation not found");
    }

    public DonationResponse createDonationResponse(Donation donation){
        DonationResponse response = new DonationResponse();
        List<ItemDonationResponse> items = new ArrayList<>();
        for (ItemDonation i : donation.getItems()){
            ItemDonationResponse itemResponse = new ItemDonationResponse();
            itemResponse.setItem_id(i.getItem_id());
            itemResponse.setItemName(i.getItemName());
            itemResponse.setQuantity(i.getQuantity());
            items.add(itemResponse);
        }
        response.setItems(items);
        if (donation.getDonor() != null) {
            response.setDonorAddress(donation.getDonor().getAddress());
            response.setDonorName(donation.getDonor().getName());
            response.setDonorPhoneNumber(donation.getDonor().getPhoneNumber());
        }
        if (donation.getRecipient() != null){
            response.setRecipientAddress(donation.getRecipient().getAddress());
            response.setRecipientName(donation.getRecipient().getName());
            response.setRecipientPhoneNumber(donation.getRecipient().getPhoneNumber());
        }
        response.setPostTitle(donation.getPostTitle());
        response.setPublishDate(new SimpleDateFormat("yyyy-MM-dd").format(donation.getPublishDate()));
        response.setStatus(donation.getStatus());
        response.setDonationId(donation.getDonationId());
        return response;
    }

    public DonationShortResponse createDonationShortResponse(Donation donation){
        DonationShortResponse response = new DonationShortResponse();
        response.setPostTitle(donation.getPostTitle());
        response.setPublishDate(new SimpleDateFormat("yyyy-MM-dd").format(donation.getPublishDate()));
        response.setStatus(donation.getStatus());
        response.setDonationId(donation.getDonationId());
        response.setDonorName(donation.getDonor().getName());
        response.setNumberItemsRequested(donation.getItems().size());
        return response;
    }
}

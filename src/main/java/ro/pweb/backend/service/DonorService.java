package ro.pweb.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.model.Donor;
import ro.pweb.backend.response.DonorResponse;
import ro.pweb.backend.repository.DonorRepository;

import javax.persistence.EntityNotFoundException;

@Service("donorService")
public class DonorService{
    @Autowired
    private DonorRepository donorRepository;

    Logger logger = LoggerFactory.getLogger(DonorService.class);

    public DonorResponse getDonorById(int donorId){
        Donor donor = donorRepository.findById(donorId).orElseThrow(() -> new NoSuchElementFoundException("Donor not found"));
        return createDonorResponse(donor);
    }

    public DonorResponse saveDonor(Donor donor){
        Donor existingDonor = donorRepository.findDonorByEmail(donor.getDonorEmail());
        if (existingDonor == null) {
            logger.info("Adding new donor");
            existingDonor = donorRepository.save(donor);
        }
        logger.info("Donor already exists");
        return createDonorResponse(existingDonor);
    }

    public DonorResponse getDonorByEmail(String email){
        Donor donor = donorRepository.findDonorByEmail(email);
        if (donor == null){
            throw new NoSuchElementFoundException("Donor not found");
        }
        return createDonorResponse(donor);

    }

    @Transactional
    public DonorResponse subscribeNewsletter(int id){
        int succes = donorRepository.subscribeNewsletter(id);
        if (succes == 1){
            return getDonorById(id);
        }
        throw new NoSuchElementFoundException("Donor not found");
    }

    @Transactional
    public DonorResponse unsubscribeNewsletter(int id){
        int succes = donorRepository.unsubscribeNewsletter(id);
        if (succes == 1){
            return getDonorById(id);
        }
        throw new NoSuchElementFoundException("Donor not found");
    }

    public boolean deleteCustomer(int donorId){
        donorRepository.deleteById(donorId);
        return true;
    }

    public DonorResponse createDonorResponse(Donor donor){
        DonorResponse response = new DonorResponse();
        response.setAddress(donor.getAddress());
        response.setPhoneNumber(donor.getPhoneNumber());
        response.setDonorEmail(donor.getDonorEmail());
        response.setSubscribedRequirements(donor.isSubscribedRequirements());
        response.setName(donor.getName());
        response.setId(donor.getId());
        return response;
    }
}

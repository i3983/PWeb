package ro.pweb.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.pweb.backend.consumer.Notification;
import ro.pweb.backend.expections.NoSuchElementFoundException;
import ro.pweb.backend.expections.OperationNotPermitedException;
import ro.pweb.backend.model.*;
import ro.pweb.backend.repository.DonorRepository;
import ro.pweb.backend.repository.RecipientRepository;
import ro.pweb.backend.response.DonationResponse;
import ro.pweb.backend.response.ItemRequirementResponse;
import ro.pweb.backend.response.RequirementResponse;
import ro.pweb.backend.repository.ItemRequirementRepository;
import ro.pweb.backend.repository.RequirementRepository;
import ro.pweb.backend.response.RequirementShortResponse;

import javax.persistence.EntityNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service("requirementService")
public class RequirementService {
    @Autowired
    private RequirementRepository requirementRepository;

    @Autowired
    private RecipientRepository recipientRepository;

    @Autowired
    private DonorRepository donorRepository;

    @Autowired
    private RabbitTemplate template;

    Logger logger = LoggerFactory.getLogger(RequirementService.class);

    public RequirementResponse saveRequirement(Requirement requirement){
        requirement.setStatus(Status.ACTIVE);
        for (ItemRequirement i : requirement.getItems()){
            i.setRequirement(requirement);
        }
        Requirement savedRequirement = requirementRepository.save(requirement);
        RequirementResponse response = createRequirementResponse(savedRequirement);
        Notification notification = new Notification("requirement", response);
        template.convertAndSend("pweb-exchange", "pweb-key", notification );
        return response;
    }

    public RequirementResponse getRequirementById(int requirementId){
        Requirement requirement = requirementRepository.findById(requirementId).orElseThrow(() -> new NoSuchElementFoundException("Requirement not found"));
        return createRequirementResponse(requirement);
    }

    public List<RequirementShortResponse> getRequirementByRecipientId(int id){
        Recipient recipient = recipientRepository.findById(id).orElseThrow(() -> new NoSuchElementFoundException("Recipient not found"));
        List<Requirement> requirements = requirementRepository.findRequirementByRecipientId(id);
        List<RequirementShortResponse> response = new ArrayList<>();
        if (requirements != null) {
            for (Requirement r : requirements) {
                response.add(createRequirementShortResponse(r));
            }
        }
        return response;
    }

    public List<RequirementShortResponse> getActiveRequirements(){
        List<Requirement> requirements = requirementRepository.findActiveRequirements();
        List<RequirementShortResponse> response = new ArrayList<>();
        for (Requirement r : requirements){
            response.add(createRequirementShortResponse(r));
        }
        return response;
    }

    public List<RequirementShortResponse> getDonorClaimedRequirements(int id){
        donorRepository.findById(id).orElseThrow(() -> new NoSuchElementFoundException("Donor not found"));
        List<Requirement> requirements = requirementRepository.findDonorClaimedRequirements(id);
        List<RequirementShortResponse> response = new ArrayList<>();
        for (Requirement r : requirements){
            response.add(createRequirementShortResponse(r));
        }
        return response;
    }

    @Transactional
    public RequirementResponse claimRequirement(Requirement requirement){
        Status requirementStatus = requirementRepository.getStatus(requirement.getRequirementId());
        if (requirementStatus != Status.ACTIVE){
            throw new OperationNotPermitedException("Requirement is not in active status");
        }
        int succes = requirementRepository.claimRequirement(requirement.getRequirementId());
        if (succes == 1){
            succes = requirementRepository.addDonor(requirement.getRequirementId(), requirement.getDonor().getId());
            return getRequirementById(requirement.getRequirementId());
        }
        throw new NoSuchElementFoundException("Requirement not found");
    }

    @Transactional
    public RequirementResponse recipientCloseRequirement(Requirement requirement){
        Status requirementStatus = requirementRepository.getStatus(requirement.getRequirementId());
        if (requirementStatus != Status.PENDING){
            throw new OperationNotPermitedException("Requirement is not in pending status");
        }
        int succes = requirementRepository.recipientCloseRequirement(requirement.getRequirementId());
        if (succes == 1){
            return getRequirementById(requirement.getRequirementId());
        }
        throw new NoSuchElementFoundException("Requirement not found");
    }


    @Transactional
    public RequirementResponse donorCloseRequirement(Requirement requirement){
        Status requirementStatus = requirementRepository.getStatus(requirement.getRequirementId());
        if (requirementStatus != Status.CLOSED_BY_RECIPIENT){
            throw new OperationNotPermitedException("Requirement is not in closed_by_recipient status");
        }
        int succes = requirementRepository.donorCloseRequirement(requirement.getRequirementId());
        if (succes == 1){
            return getRequirementById(requirement.getRequirementId());
        }
        throw new NoSuchElementFoundException("Requirement not found");
    }

    public RequirementResponse createRequirementResponse(Requirement requirement){
        RequirementResponse response = new RequirementResponse();
        List<ItemRequirementResponse> items = new ArrayList<>();
        for (ItemRequirement i :requirement.getItems()){
            ItemRequirementResponse itemResponse = new ItemRequirementResponse();
            itemResponse.setItem_id(i.getItem_id());
            itemResponse.setItemName(i.getItemName());
            itemResponse.setQuantity(i.getQuantity());
            items.add(itemResponse);
        }
        response.setItems(items);
        if (requirement.getDonor() != null) {
            response.setDonorAddress(requirement.getDonor().getAddress());
            response.setDonorName(requirement.getDonor().getName());
            response.setDonorPhoneNumber(requirement.getDonor().getPhoneNumber());
        }
        if (requirement.getRecipient() != null){
            response.setRecipientAddress(requirement.getRecipient().getAddress());
            response.setRecipientName(requirement.getRecipient().getName());
            response.setRecipientPhoneNumber(requirement.getRecipient().getPhoneNumber());
        }
        response.setPostTitle(requirement.getPostTitle());
        response.setPublishDate(new SimpleDateFormat("yyyy-MM-dd").format(requirement.getPublishDate()));
        response.setStatus(requirement.getStatus());
        response.setRequirementId(requirement.getRequirementId());
        return response;
    }

    public RequirementShortResponse createRequirementShortResponse(Requirement requirement){
        RequirementShortResponse response = new RequirementShortResponse();
        response.setPostTitle(requirement.getPostTitle());
        response.setPublishDate(new SimpleDateFormat("yyyy-MM-dd").format(requirement.getPublishDate()));
        response.setStatus(requirement.getStatus());
        response.setRequirementId(requirement.getRequirementId());
        response.setRecipientName(requirement.getRecipient().getName());
        response.setNumberItemsRequested(requirement.getItems().size());
        return response;
    }
}

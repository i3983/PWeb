FROM maven:3.8.5-jdk-11
WORKDIR /usr/src/app
COPY . .
EXPOSE 8080
RUN mvn clean install
CMD mvn spring-boot:run
